package calculadora;

import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class Controlador {
	
	private  Ventana ventana;
	private  Calculadora calculadora;
	private  String valor1;
	private  String valor2;
	private  ArrayList<Cuenta> resultados;
	private Operaciones operacion;
	private int cantidadDeCuentas;
	private int cantidadDeLibre;
	private boolean esPremium;
	
	//CONSTRUCTOR - CAMBIAR ESPREMIUM PARA PROBAR
	public Controlador() {
		this.calculadora= new Calculadora();
		resultados = new ArrayList<Cuenta>();
		esPremium=false;
		cantidadDeLibre=5;
		this.ventana = new Ventana(esPremium,cantidadDeLibre);
		this.ventana.cargaIgualListener(new CalculadoraListener());
		this.ventana.cargarVentanaResultados(new VerCuentasRealizadas());
		cantidadDeCuentas=0;
		
	}

	//SETEA LA OPERACION A REALIZAR POR CALCULADORA
	public void seteoOperador(String operador) {
		switch (operador) {
		case "-":
			setOperacion(Operaciones.RESTA);
			break;
		case "/":
			setOperacion(Operaciones.DIVISION);
			break;
		case "X":
			setOperacion(Operaciones.MULTIPLICACION);
			break;
		case "+":
			setOperacion(Operaciones.SUMA);
			break;
		default: throw new IllegalArgumentException("No existen el operador enviado");
			
		}
	}
	//VERIFICA SI ES UN OPERADOR VALIDO
	public boolean esOperador(String string) {
		return string.equals("X") || string.equals("+") || string.equals("-") || string.equals("/");
	}
	//VERIFICA QUE SEA UN NUMERO VALIDO
	public boolean esNumero(String valor) {
		boolean esNumero;
		try {
			Double.valueOf(valor);
			esNumero = true;
		}catch (NumberFormatException excepcion) {
			esNumero = false;
		}
		return esNumero;
	}
	//ACA SE REALIZA EL CALCULO PREVIO VALIDACIONES
	public void calcular(){
		if(esOperador(ventana.getOperador())) {
			seteoOperador(ventana.getOperador());
			ventana.agregarValor();
			setValor1(ventana.getValor1());
			setValor2(ventana.getValor2());
			if(esNumero(valor1) && esNumero(valor2)) {
				Double valorDoble1=Double.valueOf(valor1);
				Double valorDoble2=Double.valueOf(valor2);
				if(!(operacion.equals(Operaciones.DIVISION)&&valorDoble2.equals(0.0))) {
					Double resultado= calculadora.calcular(valorDoble1, valorDoble2, operacion);
					ventana.setearDisplay(String.valueOf(resultado));
					resultados.add(new Cuenta(valorDoble1, valorDoble2, operacion, resultado));
					cantidadDeCuentas++;
					ventana.realizoUnaCuenta();
					controlCantidadCuentas();
				}else {
					ventana.enviarAlerta("El divisor no puede ser 0. Por favor, vuelva a comenzar");
					ventana.reinicioVentana();
				}
			}else {
				ventana.enviarAlerta("Ingrese dos numeros validos. Por favor, vuelva a comenzar");
				ventana.reinicioVentana();
			}
		}
		else {
			ventana.enviarAlerta("Ingrese un operador valido. Por favor, vuelva a comenzar");
			ventana.reinicioVentana();
		}
	}
	
	//ENVIO LAS CUENTAS REALIZADAS A LA TABLA DE LA VENTANA
	public void llenoTablaCuentasRealizadas() {
		String columnas[]={"Valor1","Operador","Valor2","Resultado"};
		getVentana().getVentanaCuentas().llenoTabla(conviertoResultadosEnMatriz(), columnas);
	}
	//CONVIERTE LAS CUENTAS REALIZADAS EN EL FORMATO QUE SE NECESITA
	//PARA LLENAR LA TABLA DE LA VENTANA
	public String [][] conviertoResultadosEnMatriz() {
		String [][] matriz = new String [resultados.size()][4];
			for (int i = 0; i < matriz.length; i++) {
				resultados.get(i).toString();
				matriz[i][0]=String.valueOf(resultados.get(i).getValor1());
				matriz[i][1]=String.valueOf(resultados.get(i).getOperador());
				matriz[i][2]=String.valueOf(resultados.get(i).getValor2());
				matriz[i][3]=String.valueOf(resultados.get(i).getResultado());
			}
		return matriz;
	}
	//CONTROL CANTIDAD CUENTAS REALIZADAS MODO GRATUITO
	private void controlCantidadCuentas() {
		if((!esPremium) && cantidadDeCuentas>=cantidadDeLibre) {
			if(ventana.enviarTieneQueSerPremium()==0) {
				hacerPremium();
			}
			else {
				ventana.cerrarVentana();
			}
		}
	}
	//METODO HACE PREMIUM LA MODALIDAD
	private void hacerPremium() {
		setCantidadDeLibre(100);
		setEsPremium(true);
		ventana.activarVersionPremium();
	}
	
	public static void main(String[] args) {
		Controlador controlador = new Controlador();
	}
	//ESCUCHA BOTON IGUAL
	class CalculadoraListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				
			calcular();
				
			} catch(NumberFormatException ex) {}
			
		}
	}	
	//ESCUCHA BOTON VER CUENTAS
	class VerCuentasRealizadas implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			try {
				llenoTablaCuentasRealizadas();} catch(NumberFormatException ex) {}}
	}
	//GETERS Y SETERS
	public Operaciones getOperacion() {
		return operacion;
	}
	public void setEsPremium(boolean esPremium) {
		this.esPremium = esPremium;
	}
	public void setOperacion(Operaciones operacion) {
		this.operacion = operacion;
	}
	public int getCantidadDeCuentas() {
		return cantidadDeCuentas;
	}
	public void setCantidadDeCuentas(int cantidadDeCuentas) {
		this.cantidadDeCuentas = cantidadDeCuentas;
	}
	public int getCantidadDeLibre() {
		return cantidadDeLibre;
	}
	public void setCantidadDeLibre(int cantidadDeLibre) {
		this.cantidadDeLibre=cantidadDeLibre;
	}
	public String getValor1() {
		return valor1;
	}
	public void setValor1(String valor1) {
		this.valor1 = valor1;
	}
	public String getValor2() {
		return valor2;
	}
	public void setValor2(String valor2) {

		this.valor2 = valor2;
	}
	public Ventana getVentana() {
		return ventana;
	}
	public void setVentana(Ventana ventana) {
		this.ventana = ventana;
	}

	
}
