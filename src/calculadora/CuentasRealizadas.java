package calculadora;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextPane;
import javax.swing.JTable;

public class CuentasRealizadas {

	private JFrame frameCuentasRealizadas;
	private JTextField txtCuentasRealizadas;
	private JTable table=new JTable();;
	
	public void llenoTabla(Object[][] valores, String [] columnas){
		
        DefaultTableModel modelo = new DefaultTableModel(valores, columnas);
        table.setModel(modelo);
        table.setBounds(12, 58, 424, 201);
		frameCuentasRealizadas.getContentPane().add(table);
		SwingUtilities.updateComponentTreeUI(table);
		SwingUtilities.updateComponentTreeUI(frameCuentasRealizadas);
		

	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CuentasRealizadas window = new CuentasRealizadas();
					window.frameCuentasRealizadas.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public JFrame getFrameCuentasRealizadas() {
		return frameCuentasRealizadas;
	}

	public void setFrameCuentasRealizadas(JFrame frameCuentasRealizadas) {
		this.frameCuentasRealizadas = frameCuentasRealizadas;
	}
	
	/**
	 * Create the application.
	 */
	public CuentasRealizadas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frameCuentasRealizadas = new JFrame();
		frameCuentasRealizadas.setResizable(false);
		frameCuentasRealizadas.setAutoRequestFocus(false);
		frameCuentasRealizadas.setBounds(100, 100, 450, 300);
		frameCuentasRealizadas.getContentPane().setLayout(null);
		
		txtCuentasRealizadas = new JTextField();
		txtCuentasRealizadas.setHorizontalAlignment(SwingConstants.CENTER);
		txtCuentasRealizadas.setBounds(0, 0, 448, 59);
		txtCuentasRealizadas.setFont(txtCuentasRealizadas.getFont().deriveFont(txtCuentasRealizadas.getFont().getStyle() | Font.BOLD | Font.ITALIC));
		txtCuentasRealizadas.setText("CUENTAS REALIZADAS");
		txtCuentasRealizadas.setEditable(false);
		frameCuentasRealizadas.getContentPane().add(txtCuentasRealizadas);
		txtCuentasRealizadas.setColumns(10);
		
		
		
		
	}
}
