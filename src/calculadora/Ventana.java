package calculadora;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import java.awt.SystemColor;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.EventQueue;
import java.awt.Font;
import javax.swing.JProgressBar;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class Ventana {
	private JFrame frame;
	private JTextField display;
	private String operador="";
	private JButton botonIgual = new JButton("=");
	private JPanel panel_3 = new JPanel();
	private String valor1;
	private String valor2;
	private int cantidadCuentas;
	private UIManager.LookAndFeelInfo[] temas = UIManager.getInstalledLookAndFeels();
	private JPanel panel_2 = new JPanel();
	private JTextField txtVersinDePrueba;
	private JProgressBar progressBar = new JProgressBar();
	private CuentasRealizadas ventanaCuentas;
	private JButton verCuentasBoton = new JButton("Ver Cuentas");
	private int cantidadCuentasLibre;
	
	/**
	 * Launch the application.
	 */
	//AGREGA DE A 1 CARACTER AL DISPLAY PRINCIPAL
	public static void agregarStringDisplay (JTextField j, String string) {
	j.setText(j.getText() + string);
    }
	
	//REINICIA EL DISPLAY
	public void resetearDysplay() {
		setearDisplay("");
	}
	//REINICIA LOS VALORES 1 y 2 GUARDADOS EN VENTANA
	public void reinicioValores() {
		valor1="";
		valor2="";
	}
	//ENVIA ALERTA CON MENSAJE
	public void enviarAlerta(String mensaje) {
		JOptionPane.showMessageDialog(frame,mensaje,"Alerta",JOptionPane.OK_OPTION);
	}
	//REINICIA VENTANA (DISPLAY Y VALORES)
	public void reinicioVentana() {
		reinicioValores();
		resetearDysplay();
	}
	//REALIZA UNA CUENTA, ACTUALIZA BARRA DE CUENTAS
	public void realizoUnaCuenta() {
		reinicioValores();
		agregarValor();
		cantidadCuentas++;
		this.progressBar.setValue(cantidadCuentas);
	}
	//MENSAJE PARA SUBSCRIPCION PREMIUM
	@SuppressWarnings("deprecation")
	public int enviarTieneQueSerPremium() {
	    int resultado= JOptionPane.showConfirmDialog(null,"Para continuar usando la calculadora\n"
			    + "debe abonar la membresia.\n"
			    + "Desea abonarla?" , "Fin de periodo a prueba", JOptionPane.YES_NO_OPTION);
		return resultado;
	}
	//ACTUALIZA VENTANA CON OPCIONES PREMIUM
	public void activarVersionPremium(){
		getProgressBar().setVisible(false);
		getTxtVersinDePrueba().setText("Version Premium Desbloqueada =) ");
		getPanel_2().setVisible(true);
	}
	//ESCUCHA BOTON IGUAL
	public void cargaIgualListener(ActionListener escucharBotonCalcular) {
		
		botonIgual.addActionListener(escucharBotonCalcular);
		
	}
	//ESCUCHA BOTON VER CUENTAS REALIZADAS
	public void cargarVentanaResultados(ActionListener VerCuentasRealizadas) {
		
		verCuentasBoton.addActionListener(VerCuentasRealizadas);
		
	}
	//AGREGA VALOR, LOS VA CORRIENDO, Y SE VA QUEDANDO CON LOS ULT 2
	public void agregarValor() {
		if(valor1.length()==0) {
			setValor1(display.getText());
		}else if (valor2.length()==0) {
			setValor2(display.getText());
		}else {
			setValor1(getValor2());
			setValor2(display.getText());
		}
	}
	//OBTIENE LOS NOMBRES DE L&F PARA MOSTRAR EN SELECT
	public String [] nombresLookAndFeel() {
		String [] nombres = new String [temas.length];
		for (int i = 0; i < temas.length; i++) {
			nombres[i]=temas[i].getName();
		}
			return nombres;
	}
	//OBTIENE LOS NOMBRES DE CLASE DE L&F 
	public void seleccionarLookAndFeel(String string) {
		String lookAndFeelClassName="";
		for (int i = 0; i < temas.length; i++) {
			if(string.equals(temas[i].getName())) {
				lookAndFeelClassName=temas[i].getClassName();
			}
		}
		try {
			UIManager.setLookAndFeel(lookAndFeelClassName);
			SwingUtilities.updateComponentTreeUI(frame);

        } catch(Exception e) {
            e.printStackTrace();
        }
	}
	//ELIMINA ULT CARACTER DISPLAY
	public void eleminarCaracterDisplay() {
		if(display.getText().length()>0)display.setText((display.getText().substring(0, display.getText().length()-1)));
	}
	//CIERRA VENTANA
	public void cerrarVentana(){
		getFrame().dispose();
	}


	/**
	 * Create the application.
	 */
	public Ventana(boolean espremium,int cantidadCuentasLibre) {
		valor1="";
		valor2="";
		cantidadCuentas=0;
		initialize();
		this.cantidadCuentasLibre = cantidadCuentasLibre;
		progressBar.setMaximum(cantidadCuentasLibre);
		ventanaCuentas = new CuentasRealizadas();
		if(espremium) activarVersionPremium();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 509, 417);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.getContentPane().setLayout(null);
		
		JPanel panelNumeros = new JPanel();
		panelNumeros.setBounds(22, 85, 264, 253);
		frame.getContentPane().add(panelNumeros);
		GridLayout gl = new GridLayout(4,3);
		gl.setHgap(5); gl.setVgap(5);
		panelNumeros.setLayout(gl);
		
		JButton boton7 = new JButton("7");
		boton7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarStringDisplay(display, "7");
			}
		});
		panelNumeros.add(boton7);
		
		JButton boton8 = new JButton("8");
		boton8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarStringDisplay(display, "8");
			}
		});
		panelNumeros.add(boton8);
		
		JButton boton9 = new JButton("9");
		boton9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarStringDisplay(display, "9");
			}
		});
		panelNumeros.add(boton9);
	
		JButton boton4 = new JButton("4");
		boton4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarStringDisplay(display, "4");
			}
		});
		
		JButton botonSumar = new JButton("+");
		botonSumar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setOperador("+");
				agregarValor();
				resetearDysplay();
			}
		});
		panelNumeros.add(botonSumar);
		panelNumeros.add(boton4);
		
		JButton boton5 = new JButton("5");
		boton5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarStringDisplay(display, "5");
			}
		});
		panelNumeros.add(boton5);
		
		JButton boton6 = new JButton("6");
		boton6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarStringDisplay(display, "6");
			}
		});
		panelNumeros.add(boton6);
		
		JButton botonMultiplicacion = new JButton("X");
		botonMultiplicacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setOperador("X");
				agregarValor();
				resetearDysplay();
			}
		});
		panelNumeros.add(botonMultiplicacion);
		
	
		JButton boton1 = new JButton("1");
		boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarStringDisplay(display, "1");
				}
		});
		panelNumeros.add(boton1);
		
		JButton botonResta = new JButton("-");
		botonResta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setOperador("-");
				agregarValor();
				resetearDysplay();
			}
		});
		
		JButton boton2 = new JButton("2");
		boton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarStringDisplay(display, "2");
			}
		});
		panelNumeros.add(boton2);
		
		JButton boton3 = new JButton("3");
		boton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarStringDisplay(display, "3");
			}
		});
		panelNumeros.add(boton3);
		panelNumeros.add(botonResta);
		
		JButton botonPunto = new JButton(".");
		botonPunto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarStringDisplay(display, ".");
			}
		});
		
		
		
		JButton boton0 = new JButton("0");
		boton0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarStringDisplay(display, "0");
			}
		});
		panelNumeros.add(boton0);
		panelNumeros.add(botonPunto);
		
		JButton botonDivision = new JButton("/");
		botonDivision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setOperador("/");
				agregarValor();
				resetearDysplay();
			}
		});
		
		botonIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		panelNumeros.add(botonIgual);
		
		panelNumeros.add(botonDivision);
		
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(22, 12, 456, 52);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		display = new JTextField();
		display.setEditable(false);
		display.setFont(new Font("Chandas", Font.PLAIN, 18));
		display.setBackground(SystemColor.controlHighlight);
		display.setBounds(12, 5, 432, 44);
		panel_1.add(display);
		display.setColumns(10);
		
		JPanel panel = new JPanel();
		panel.setBounds(298, 209, 178, 126);
		frame.getContentPane().add(panel);
		panel.setLayout(new GridLayout(2, 1, 0, 0));
		
		JButton reiniciar = new JButton("Reiniciar");
		reiniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				reinicioVentana();
			}
		});
		panel.add(reiniciar);
		
		JButton Salir = new JButton("Salir");
		Salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {	
				
				cerrarVentana();
			}
		});
		panel.add(Salir);
		//panel_2.setVisible(false);
		panel_2.setBounds(298, 85, 178, 120);
		frame.getContentPane().add(panel_2);
		panel_2.setLayout(new GridLayout(3, 0, 0, 0));
		panel_2.setVisible(false);
		JComboBox combo = new JComboBox();
		combo.setModel(new DefaultComboBoxModel(nombresLookAndFeel()));
		panel_2.add(combo);
		combo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				seleccionarLookAndFeel(combo.getSelectedItem().toString());
			}
		});
		panel_2.add(verCuentasBoton);
		
		
		verCuentasBoton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							CuentasRealizadas window = new CuentasRealizadas();
							ventanaCuentas.getFrameCuentasRealizadas().setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		
		JPanel panel_4 = new JPanel();
		panel_2.add(panel_4);
		panel_4.setLayout(new GridLayout(0, 3, 0, 0));
		
		JButton botonBorrar = new JButton("<-");
		botonBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				eleminarCaracterDisplay();
			}
		});
		panel_4.add(botonBorrar);
		panel_3.setBounds(22, 337, 456, 51);
		frame.getContentPane().add(panel_3);
		panel_3.setLayout(new GridLayout(2, 1, 0, 0));
		txtVersinDePrueba = new JTextField();
		txtVersinDePrueba.setText("Version de Prueba");
		txtVersinDePrueba.setBackground(SystemColor.window);
		panel_3.add(txtVersinDePrueba);
		txtVersinDePrueba.setColumns(10);
		panel_3.add(progressBar);
		this.getFrame().setVisible(true);
		
			
	}
	
	

	public void setProgressBar(JProgressBar progressBar) {
		this.progressBar = progressBar;
	}

	public JPanel getPanel_3() {
		return panel_3;
	}

	public void setPanel_3(JPanel panel_3) {
		this.panel_3 = panel_3;
	}

	public JPanel getPanel_2() {
		return panel_2;
	}

	public void setPanel_2(JPanel panel_2) {
		this.panel_2 = panel_2;
	}
	public JTextField getTxtVersinDePrueba() {
		return txtVersinDePrueba;
	}

	public void setTxtVersinDePrueba(JTextField txtVersinDePrueba) {
		this.txtVersinDePrueba = txtVersinDePrueba;
	}
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	
	public String getOperador() {
		return operador;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}
	
	public JTextField getDisplay() {
		return display;
	}


	public void setDisplay(JTextField display) {
		this.display = display;
	}
	public String getValor1() {
		return valor1;
	}

	public void setValor1(String valor1) {
		this.valor1 = valor1;
	}

	public String getValor2() {
		return valor2;
	}

	public void setValor2(String valor2) {
		this.valor2 = valor2;
	}
	
	//GETERS & SETERS
	public void setearDisplay( String string) {
		getDisplay().setText(string);
	}
	public CuentasRealizadas getVentanaCuentas() {
		return ventanaCuentas;
	}
	public JProgressBar getProgressBar() {
		return progressBar;
	}

}
