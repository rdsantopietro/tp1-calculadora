package calculadora;

public class Cuenta {
	private Double valor1;
	private Double valor2;
	private Operaciones operador;
	
	private Double resultado;
	// REALIZA CUENTA
	public Cuenta(Double valor1, Double  valor2,Operaciones operador, Double resultado ) {
		this.valor1 = valor1;
		this.valor2 = valor2;
		this.resultado = resultado;
		this.operador=operador;
	}
	//TOSTRING
	@Override
	public String toString() {
		return String.valueOf(valor1) + " " + String.valueOf(operador.caracter() + " " + String.valueOf(valor2) + " = " + String.valueOf(resultado) ) ;   
	}
	
	
	
	//GETES Y SETERS
	public Double getValor1() {
		return valor1;
	}
	public void setValor1(Double valor1) {
		this.valor1 = valor1;
	}
	public Double getValor2() {
		return valor2;
	}
	public void setValor2(Double valor2) {
		this.valor2 = valor2;
	}
	public Operaciones getOperador() {
		return operador;
	}
	public void setOperador(Operaciones operador) {
		this.operador = operador;
	}
	public Double getResultado() {
		return resultado;
	}
	public void setResultado(Double resultado) {
		this.resultado = resultado;
	}
}
