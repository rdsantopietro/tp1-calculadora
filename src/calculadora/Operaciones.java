package calculadora;

public enum Operaciones {
	RESTA, DIVISION, SUMA, MULTIPLICACION;

	public String caracter() {
		switch (this) {
		case RESTA: return "-";
		case SUMA: return "+";
		case DIVISION: return "/";
		case MULTIPLICACION: return "X";
		default:
			return "";
		}
		
	}
}
