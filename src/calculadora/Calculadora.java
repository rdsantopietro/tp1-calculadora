package calculadora;

public class Calculadora {
	
	public Double calcular(Double valor1,Double valor2, Operaciones operador) {
		switch (operador) {
		case SUMA:
			return sumar(valor1, valor2);
		case DIVISION:
			if(valor2==0){
				throw new IllegalArgumentException("La division por 0 no esta definida");
			}
			return dividir(valor1, valor2);
		case MULTIPLICACION:
			return multiplicar(valor1, valor2);
		case RESTA:
			return restar(valor1, valor2);
		default: return 0.0;	

		}
	}
	 

	public Double dividir(Double valor1 , Double valor2) {
		return valor1/valor2;
	}
	public Double sumar(Double valor1 , Double valor2) {
		return valor1+valor2;
	}
	public Double multiplicar(Double valor1 , Double valor2) {
		return valor1*valor2;
	}
	public Double restar(Double valor1 , Double valor2) {
		return valor1-valor2;
	}
}
